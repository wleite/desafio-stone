import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FuncionarioComponent } from './funcionario/funcionario.component';
import { FuncionarioFormComponent } from './funcionario/funcionario-form/funcionario-form.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: FuncionarioComponent },
  { path: 'funcionario_new', component: FuncionarioFormComponent},
  { path: 'funcionario/:id', component: FuncionarioFormComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
