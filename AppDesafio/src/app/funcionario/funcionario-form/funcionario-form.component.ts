import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/model/funcionario';
import { Router, ActivatedRoute } from '@angular/router';
import { FuncionarioService } from 'src/app/service/funcionario.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

@Component({
  selector: 'app-funcionario-form',
  templateUrl: './funcionario-form.component.html',
  styleUrls: ['./funcionario-form.component.css']
})
export class FuncionarioFormComponent implements OnInit {

  title: string;
  funcionario: Funcionario = new Funcionario();
  form:FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,  
              private route: ActivatedRoute,  
              private _service: FuncionarioService
              ) {
                this.form = this.formBuilder.group({
                  nome: ['', Validators.required],
                  idade: ['', Validators.required],
                  cargo: ['', Validators.required]
              });
  }

  get f() { return this.form.controls; }

  ngOnInit() {
    var id = this.route.params.subscribe(params => {
      var id = params['id'];
      this.title = id ? 'Editando Funcionário' : 'Novo Funcionario';

      if (!id)
        return;

      this._service.getOne(id)
        .then(res => this.funcionario = res as Funcionario)
        .catch(res => {
            if (res.grupo == 404) {
              this.router.navigate(['NotFound']);
            }
        });
    });
  }

  
  salvar() {
    this.submitted = true;
    var result;     
    if(!(this.f.nome.errors || this.f.idade.errors || this.f.cargo.errors)){
      if (this.funcionario.id){        
        result = this._service.update(this.funcionario);
      } else {
        result = this._service.save(this.funcionario);
      }  
      result.then(res =>{       
        this.router.navigate(["/"])
      });
    }
  }

}

