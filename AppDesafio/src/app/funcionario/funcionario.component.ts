import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FuncionarioService } from '../service/funcionario.service';
import { Funcionario } from '../model/funcionario';

@Component({
  selector: 'app-funcionario',
  templateUrl: './funcionario.component.html',
  styleUrls: ['./funcionario.component.css']
})
export class FuncionarioComponent implements OnInit {
    
   funcionarios: Funcionario[] = [];

   constructor(private router: Router,    
     private _service: FuncionarioService
     ) {     
   }
 
   ngOnInit() { 
     this.listar();      
   }
 
   listar(){  
     this._service.getAll().then(
       res =>{             
               this.funcionarios = res as Funcionario[];  
             } 
     )             
     .catch(res => {
         if (res.status == 404) {
           this.router.navigate(['NotFound']);
         }
     });      
   }  
 
 
   remover(funcionario){
    if (confirm("Confirma a exclusão do Funcionario " + funcionario.nome + "?")) {
      var index = this.funcionarios.indexOf(funcionario);
      this.funcionarios.splice(index, 1);
      this._service.remove(funcionario.id)        
        .then(res =>{                
          this.router.navigate(['/']);          
         }
       ) 
       .catch(
         res => alert('Este Funcionário não pode ser excluido!Contate o administrador do sistema.')
       )      
     }
   }
 
 }