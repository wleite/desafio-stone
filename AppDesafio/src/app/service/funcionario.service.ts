import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})

export class FuncionarioService {

  headers = new HttpHeaders({
    "Content-Type": "application/json",
    "Accept": "application/json"
  });

  url: string = "http://localhost:8000/api/";

  constructor(public _http:HttpClient) {}
  
  getAll(){  
    return this._http.get(this.url+'funcionarios')
    .toPromise()
    .then(res => res);
  }

  getOne(id){    
    return this._http.get(this.url+'funcionario/'+id)
    .toPromise()
    .then(res => res)    
  }

  save(obj){  
    return this._http.post(this.url+'funcionario',obj)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  update(obj){      
    return this._http.put(this.url+'funcionario',obj)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  remove(id){
    console.log(this.url+'funcionario/'+id);
    return this._http.delete(this.url+'funcionario/'+id)
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }
 
  handleError(error: any): Promise<any> {
    console.error('Problema na operação realizada!', error);
    return Promise.reject(error.message || error);
  } 
}