// Create express app
var express = require("express")
var cors = require('cors')
var app = express()
var db = require("./src/database/database.js")
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Server port
var HTTP_PORT = 8000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"DesafioApi":"Seja bem vindo"})
});

app.use(cors());

app.get("/api/funcionarios", (req, res, next) => {
    var sql = "select * from funcionario"
    var params = []
    db.all(sql, params, (err, rows) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json(rows)
      });
});

app.get("/api/funcionario/:id", (req, res, next) => {
    var sql = "select * from Funcionario where id = ?"
    var params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json(row)
      });
})

app.post("/api/funcionario", (req, res, next) => {
    var errors=[]
    if (!req.body.nome){
        errors.push("Nome obrigatorio");
    }
    if (!req.body.idade){
        errors.push("Idade obrigatoria");
    }
    if (!req.body.cargo){
        errors.push("Cargo obrigatorio");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
       
        nome: req.body.nome,
        idade: req.body.idade,
        cargo : req.body.cargo
    }
    var sql ='INSERT INTO Funcionario (nome, idade, cargo) VALUES (?,?,?)'
    var params =[data.nome, data.idade, data.cargo]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json(data)
    });
})

app.put("/api/funcionario", (req, res, next) => {
    var data = {
        id: req.body.id,
        nome: req.body.nome,
        idade: req.body.idade,
        cargo : req.body.cargo
    }
    db.run(
        `UPDATE Funcionario set 
           nome = COALESCE(?,nome), 
           idade = COALESCE(?,idade), 
           cargo = COALESCE(?,cargo) 
           WHERE id = ?`,
        [data.nome, data.idade, data.cargo, data.id],
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})

app.delete("/api/funcionario/:id", (req, res, next) => {
    db.run(
        'DELETE FROM Funcionario WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
    });
})

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});