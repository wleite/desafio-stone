var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      console.error(err.message)
      throw err
    }else{
        console.log('Conectado ao banco de dados.')
        db.run(`CREATE TABLE Funcionario (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            nome text NOT NULL, 
            idade integer NOT NULL,
            cargo text NOT NULL          
            )`,
        (err) => {
            if (err) {
                console.log('Erro no banco de dados')
            }
        });  
    }
});

module.exports = db